package amarjothi.in.qrscan.interfaces;

/**
 * Created by arunkumar on 24-07-2015.
 */
public interface AsyncResponseSave {
    void processFinish(String output);
}
