package amarjothi.in.qrscan;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import amarjothi.in.qrscan.DBManagement.DBManager;
import amarjothi.in.qrscan.DBManagement.DBManagerActivity;
import amarjothi.in.qrscan.QR.QRView;
import amarjothi.in.qrscan.dbconnection.MysqlConnection;
import amarjothi.in.qrscan.dbconnection.OracleDBConnection;
import amarjothi.in.qrscan.interfaces.AsyncResponseLogin;
import amarjothi.in.qrscan.models.MachineDataModel;
import amarjothi.in.qrscan.models.ProcessTypeModel;
import amarjothi.in.qrscan.util.Common;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class Login extends AppCompatActivity implements AsyncResponseLogin,View.OnClickListener{

    EditText username,password,cameraposition;
    Button Submit,DBConfig;
    JSONObject json;
    JSONObject jsonArray;
    private String url_login ;
    Credentials login;
    Common common;
    private DBManager dbManager;
    private  int MY_STORAGE_REQUEST_CODE = 6514;
    ArrayList<ProcessTypeModel> ProcessTypeList = new ArrayList();
    ArrayList<MachineDataModel> MachineTypeList = new ArrayList();
    // Check local oracle db configured with sqllite
    boolean localoracledb = true;
    Cursor cursor;
    Connection conLogin ;

    String dbHost;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewsById();
        Submit.setOnClickListener(this);
        //DBConfig.setOnClickListener(this);
        common = new Common();
        dbManager = new DBManager(this);
        dbManager.open();

        // Check local oracle connection availabily . If not available connect to remove mysql server
        //localoracledb = CheckLocalOracleConnectionAvail();

    }
    protected void onStart() {
        super.onStart();
        Log.e("onStart","Onstart");
        if(conLogin != null){
            Log.e("login onstart",conLogin.toString());
        }
        //localoracledb = CheckLocalOracleConnectionAvail();
        //dbManager.open();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        try
        { if (conLogin != null) conLogin.close();} catch (Exception ex){ex.printStackTrace();}

    }

    @Override
    protected void onResume() {
        super.onResume();
        // set connection to null to create new connection after logout.
        OracleDBConnection.con = null;
        Log.e("onResume","onResume");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Checks permission for using camera
            // if Not asks for Permission
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                String[] st = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
                ActivityCompat.requestPermissions(this, st,MY_STORAGE_REQUEST_CODE );
                return;
            }
        }

    }

    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.button):
                Log.e("Login","Login");
                login = new Credentials();
                login.delegate=this;
                login.execute();
                break;
            case (R.id.bDBConfig):
                Log.e("db confog","dbf config");
//            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Intent DBManager = new Intent(Login.this, DBManagerActivity.class);
                Bundle b = new Bundle();
                Log.e("db confog",Boolean.toString(localoracledb));
                if(localoracledb) {
                    b.putString("dbHost", cursor.getString(1));
                }
                else {
                    b.putString("dbHost", "Remote Mysql");
                }
                DBManager.putExtras(b);
                startActivity(DBManager);
                break;
        }
    }
    public void processFinish(Integer output){
        //this you will received result fired from async class of onPostExecute(result) method.
        Log.e("result",""+output);
        if(output==0) {
            common.createCustomToast(getApplicationContext(),"Communication Error / User Not Available", (ViewGroup)findViewById(R.id.toast_layout_root),R.drawable.man_50);
        }
        else {
            // Menu activity removed . Next screen changed to QRScan
            //Intent login = new Intent(Login.this, MenuActivity.class);
            Intent login = new Intent(Login.this, QRView.class);
            Bundle b = new Bundle();
            b.putInt("userid",output);
            b.putInt("cameraposition",Integer.parseInt(cameraposition.getText().toString()));
            b.putString("username",username.getText().toString());
            b.putBoolean("localoracledb",localoracledb);
            b.putSerializable("processtypelist",ProcessTypeList);
            b.putSerializable("machinetypelist",MachineTypeList);
            login.putExtras(b);
//            login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);
        }
    }


    private void findViewsById() {
        Submit = (Button) findViewById(R.id.button);
        DBConfig =  findViewById(R.id.bDBConfig);
        username = (EditText) findViewById(R.id.ETusername);
        password = (EditText) findViewById(R.id.ETpassword);
        cameraposition = (EditText) findViewById(R.id.ETCamera);
    }


    private class Credentials extends AsyncTask<String, String, Integer> {
        public AsyncResponseLogin delegate=null;
        String sUserName = null;
        String sPassWord = null;
        int userId = 0;
        @Override
        protected void onPreExecute(){
            sUserName = username.getText().toString();
            sPassWord = password.getText().toString();
        }
        protected void onPostExecute(Integer result) {
            delegate.processFinish(result);
            Log.e("asnyc result",""+result);
        }

        protected Integer doInBackground(String... args) {
            Log.e("login" , "Under Background");
            ArrayList<HashMap<String, Object>> MachList = new ArrayList<HashMap<String, Object>>();

            conLogin =  null;
            ResultSet res=null, res1 = null ,res2 = null;
            PreparedStatement ps=null, ps1= null, ps2= null;

            try {
                if (localoracledb) {
                    // conLogin = getOracleConnection();
                    conLogin = OracleDBConnection.getConnection("dummy");
                    Log.e("db conect", conLogin.toString());
                }
                else{
                    conLogin = MysqlConnection.getConnection();
                }
                Log.e("conect",conLogin.toString());
                Log.e("user name ","xx"+sUserName.length()+" password "+sPassWord.length());

                String sqlCredentails  =       " select id,username,password from  tbl_qrcode_user where username =  ? and password = ?";
                String sqlProcessType  =       " Select Processid,ProcessName,batchnoflag from devicemast order by processname";
                //StringBuilder sqlMachineType  = new StringBuilder("select   Machinmast.machinmastid, Machinmast.MachineId,Machinmast.grpname,Machinmast.processid,devicemast.processid from Machinmast ") ;
                //sqlMachineType.append(" left join devicemast on Machinmast.grpname= devicemast.processname ");
                StringBuilder sqlMachineType  = new StringBuilder(" select Machinmast.machinmastid, Machinmast.MachineId,Machinmast.grpname from Machinmast ") ;
                sqlMachineType.append(" left join devicemast on Machinmast.grpname= devicemast.processname ");

                if (sUserName.length()>0 && sPassWord.length()>0){
                    ps = conLogin.prepareStatement(sqlCredentails);
                    Log.e("user name ",""+sUserName);
                    ps.setString (1,sUserName);
                    ps.setString(2,sPassWord);
                    res = ps.executeQuery();
                }
                else{
                    return 0;
                }
                while (res.next()) {
                    userId  = res.getInt(1) ;
                }
                ps1 = conLogin.prepareStatement(sqlProcessType);
                res1 = ps1.executeQuery();
                while (res1.next()) {
                    ProcessTypeModel processTypeModel = new ProcessTypeModel();
                    processTypeModel.setProcessid(res1.getString(1));
                    Log.e("processid ",""+res1.getDouble(1));
                    processTypeModel.setProcessname(res1.getString(2));
                    processTypeModel.setBatchnoflag(res1.getInt(3));
                    ProcessTypeList.add(processTypeModel);
                }

                ps2 = conLogin.prepareStatement(sqlMachineType.toString());
                res2 = ps2.executeQuery();
                while (res2.next()) {
                    MachineDataModel machineModel = new MachineDataModel();
                    machineModel.setMachinmastid(res2.getString(1));
                    machineModel.setMachineid(res2.getString(2));
                    machineModel.setGrpname(res2.getString(3));
                //    machineModel.setMachineprocessid(res2.getString(4));
//                    machineModel.setProcessprocessid(res2.getString(5));
                    MachineTypeList .add(machineModel);
                }

            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            finally {
                try
                {   if (res!= null){
                    res.close();
                }
                    if (res1!= null) {
                        res1.close();
                    }
                } catch (Exception ex){ex.printStackTrace();}
            }
            Log.e("user id ",""+userId);
            return userId;
        }
    }
    private boolean CheckLocalOracleConnectionAvail(){
        cursor = dbManager.fetch();
        Log.e("auto id "," none ");
        if (cursor.getCount()>0) {
            cursor.moveToFirst();
            Log.e("auto id fetch ",cursor.getString(0));
            return true;
        }
        return false;
    }
    private Connection getOracleConnection(){
        //public void insert(String ip, int port, String servicename, String username,String password) {
        Connection connection = null;
        String username =  cursor.getString(4);
        dbHost =  cursor.getString(1); ;
        String dbName =  cursor.getString(3);;
        int ServerPort = cursor.getInt(2);;
        String password =  cursor.getString(5);;
        Log.e("dbHost ", dbHost);
        try {
            connection = OracleDBConnection.getConnection(dbHost, ServerPort, dbName, username, password);
            Log.e("getOracleconnection ", connection.toString());
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return connection;
    }
}
