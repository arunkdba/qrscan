package amarjothi.in.qrscan.DBManagement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DBManager {
    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBManager(Context c) {
        context = c;
    }

    public DBManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String ip, int port, String servicename, String username,String password) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper.SERVERIP, ip);
        contentValue.put(DatabaseHelper.PORT, port);
        contentValue.put(DatabaseHelper.SERVICENAME, servicename);
        contentValue.put(DatabaseHelper.USERNAME, username);
        contentValue.put(DatabaseHelper.PASSWORD, password);
        database.insert(DatabaseHelper.TABLE_NAME, null, contentValue);
    }

    public Cursor fetch() {
        String[] columns = new String[] { DatabaseHelper._ID, DatabaseHelper.SERVERIP, DatabaseHelper.PORT, DatabaseHelper.SERVICENAME,
                DatabaseHelper.USERNAME, DatabaseHelper.PASSWORD};
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }
/*
    public int update(String bagno, int instruction, Boolean selected) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.SELECTED, selected);
        int i = database.update(DatabaseHelper.TABLE_NAME, contentValues, DatabaseHelper.BAGNO + " = '" + bagno +"' and "+  DatabaseHelper.INSTRUCTION + " = " + instruction , null);
        return i;
    }
*/
    public void delete(long _id) {
        database.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper._ID + "=" + _id, null);
    }

    public void deleteAll() {
        database.delete(DatabaseHelper.TABLE_NAME, null, null);
    }
/*
    public void deleteInstruction(int InstructionNo) {             // Once bags saved for Instruction , bags will be deleted from Local sqlite DB permanently
        database.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper.INSTRUCTION + "=" + InstructionNo, null);
    }
*/
}
