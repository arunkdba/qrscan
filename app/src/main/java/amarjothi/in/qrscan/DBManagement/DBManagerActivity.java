package amarjothi.in.qrscan.DBManagement;

import amarjothi.in.qrscan.util.Common;
import amarjothi.in.qrscan.R;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class DBManagerActivity extends AppCompatActivity implements View.OnClickListener{
    // Database to store Oracle DB Details
    private DBManager dbManager;
    EditText etUsername,etPassword,etServerip , etServerport,etSericename;
    TextView etServerDet;
    Button bSave;
    Common common = new Common();
    Bundle bundle;
    String dbHost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbmanager);
        bundle = getIntent().getExtras();
        dbHost = bundle.getString("dbHost");

        findViewsById();
        bSave.setOnClickListener(this);
    }
    private void findViewsById() {
        bSave =  findViewById(R.id.bSave);
        etUsername =  findViewById(R.id.etUserName);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etServerip = (EditText) findViewById(R.id.etServerIP);
        etServerport = (EditText) findViewById(R.id.etServerPort);
        etSericename = (EditText) findViewById(R.id.etServiceName);
        etServerDet = (TextView) findViewById(R.id.etServerDet);
        etServerDet.setText("DB HOST :"+ dbHost);
    }
    public void onClick(View v) {
        saveDBConfg();
        common.createCustomToast(getApplicationContext(), "DB Config Saved", (ViewGroup) findViewById(R.id.toast_layout_root), R.drawable.man_50);
    }
    private void saveDBConfg(){
        dbManager = new DBManager(this);
        dbManager.open();
        dbManager.deleteAll();
        //public void insert(String ip, int port, String servicename, String username,String password) {
        if (etServerip.getText().toString().length()>0 &&  etServerport.getText().toString().length()>0
                && etSericename.getText().toString().length()>0 && etUsername.getText().toString().length()>0
                && etPassword.getText().toString().length()>0){
            dbManager.insert(etServerip.getText().toString(), Integer.parseInt(etServerport.getText().toString())
                    , etSericename.getText().toString(), etUsername.getText().toString(), etPassword.getText().toString());
            Cursor cursor = dbManager.fetch();
            Log.e("server ip ",cursor.getString(1));
        }
    }
}

/*
    public Cursor fetch() {
        String[] columns = new String[] { DatabaseHelper._ID, DatabaseHelper.BAGNO, DatabaseHelper.INSTRUCTION, DatabaseHelper.SELECTED};
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }
*/