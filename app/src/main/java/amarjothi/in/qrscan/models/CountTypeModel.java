package amarjothi.in.qrscan.models;

import java.io.Serializable;

public class CountTypeModel implements Serializable {
    String CountsMastid;
    String CountsId;

    public String getCountsMastid() {
        return CountsMastid;
    }

    public void setCountsMastid(String countsMastid) {
        CountsMastid = countsMastid;
    }

    public String getCountsId() {
        return CountsId;
    }

    public void setCountsId(String countsId) {
        CountsId = countsId;
    }

    @Override
    public String toString() {
        return CountsId;
    }

}
