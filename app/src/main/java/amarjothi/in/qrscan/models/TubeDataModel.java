package amarjothi.in.qrscan.models;

public class TubeDataModel {
    String tubemastid,tubename;
    double tubewt;

    public String getTubemastid() {
        return tubemastid;
    }

    public void setTubemastid(String tubemastid) {
        this.tubemastid = tubemastid;
    }

    public String getTubename() {
        return tubename;
    }

    public void setTubename(String tubename) {
        this.tubename = tubename;
    }

    public double getTubewt() {
        return tubewt;
    }

    public void setTubewt(double tubewt) {
        this.tubewt = tubewt;
    }

    @Override
    public String toString() {
        return ""+tubewt;
    }
}
