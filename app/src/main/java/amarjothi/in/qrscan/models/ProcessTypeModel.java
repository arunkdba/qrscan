package amarjothi.in.qrscan.models;

import android.os.Parcelable;

import java.io.Serializable;

public class ProcessTypeModel implements Serializable {
    String processid;
    String processname;
    // To filter machine based on batchnoflag;
    // if 1 then machineno to be selected based on the jobcem table details
    int batchnoflag;

    public int getBatchnoflag() {
        return batchnoflag;
    }

    public void setBatchnoflag(int batchnoflag) {
        this.batchnoflag = batchnoflag;
    }

    public String getProcessid() {
        return processid;
    }

    public void setProcessid(String processid) {
        this.processid = processid;
    }

    public String getProcessname() {
        return processname;
    }

    public void setProcessname(String processname) {
        this.processname = processname;
    }

    @Override
    public String toString() {
        return processname;
    }
}
