package amarjothi.in.qrscan.models;

import java.io.Serializable;

public class YarnTypeModel implements Serializable {
    String yarntypemastid,yarntype;

    public String getYarntypemastid() {
        return yarntypemastid;
    }

    public void setYarntypemastid(String yarntypemastid) {
        this.yarntypemastid = yarntypemastid;
    }

    public String getYarntype() {
        return yarntype;
    }

    public void setYarntype(String yarntype) {
        this.yarntype = yarntype;
    }

    @Override
    public String toString() {
        return yarntype;
    }
}
