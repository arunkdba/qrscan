package amarjothi.in.qrscan.models;

import java.io.Serializable;

public class MachineDataModel implements Serializable {
    // machineprocessid  :  machine which do this process
    // processprocessid : Process id in process table devicemast table
    String machinmastid,machineprocessid,processprocessid;
    String machineid,grpname;

    public String getMachinmastid() {
        return machinmastid;
    }

    public void setMachinmastid(String machinmastid) {
        this.machinmastid = machinmastid;
    }

    public String getMachineprocessid() {
        return machineprocessid;
    }

    public void setMachineprocessid(String machineprocessid) {
        this.machineprocessid = machineprocessid;
    }

    public String getProcessprocessid() {
        return processprocessid;
    }

    public void setProcessprocessid(String processprocessid) {
        this.processprocessid = processprocessid;
    }

    public String getMachineid() {
        return machineid;
    }

    public void setMachineid(String machineid) {
        this.machineid = machineid;
    }

    public String getGrpname() {
        return grpname;
    }

    public void setGrpname(String grpname) {
        this.grpname = grpname;
    }

    public String toString() {
        return machineid;
    }

}
