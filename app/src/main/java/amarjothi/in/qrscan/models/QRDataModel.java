package amarjothi.in.qrscan.models;

public class QRDataModel {
    String batchno,filcust,filcounts,mcname,mctype,labdipno,btn,totbqty,qrcodedata,jobcemid,color;
    double partyid,macno;
    double colourid;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getMacno() {
        return macno;
    }

    public void setMacno(double macno) {
        this.macno = macno;
    }

    public String getBatchno() {
        return batchno;
    }

    public void setBatchno(String batchno) {
        this.batchno = batchno;
    }

    public double getPartyid() {
        return partyid;
    }

    public void setPartyid(double partyid) {
        this.partyid = partyid;
    }

    public String getBatcno() {
        return batchno;
    }

    public void setBatcno(String batcno) {
        this.batchno = batcno;
    }

    public String getFilcust() {
        return filcust;
    }

    public void setFilcust(String filcust) {
        this.filcust = filcust;
    }

    public double getColourid() {
        return colourid;
    }

    public void setColourid(double colorid) {
        this.colourid = colorid;
    }

    public String getFilcounts() {
        return filcounts;
    }

    public void setFilcounts(String filcounts) {
        this.filcounts = filcounts;
    }

    public String getMcname() {
        return mcname;
    }

    public void setMcname(String mcname) {
        this.mcname = mcname;
    }

    public String getMctype() {
        return mctype;
    }

    public void setMctype(String mctype) {
        this.mctype = mctype;
    }

    public String getLabdipno() {
        return labdipno;
    }

    public void setLabdipno(String labdipno) {
        this.labdipno = labdipno;
    }

    public String getBtn() {
        return btn;
    }

    public void setBtn(String btn) {
        this.btn = btn;
    }

    public String getTotbqty() {
        return totbqty;
    }

    public void setTotbqty(String totbqty) {
        this.totbqty = totbqty;
    }

    public String getQrcodedata() {
        return qrcodedata;
    }

    public void setQrcodedata(String qrcodedata) {
        this.qrcodedata = qrcodedata;
    }

    public String getJobcemid() {
        return jobcemid;
    }

    public void setJobcemid(String jobcemid) {
        this.jobcemid = jobcemid;
    }
}
