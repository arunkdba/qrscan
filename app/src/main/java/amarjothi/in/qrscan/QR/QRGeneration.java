package amarjothi.in.qrscan.QR;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import amarjothi.in.qrscan.R;
import amarjothi.in.qrscan.dbconnection.MysqlConnection;
import amarjothi.in.qrscan.interfaces.AsyncResponseSave;
import amarjothi.in.qrscan.util.Common;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class QRGeneration extends AppCompatActivity implements AsyncResponseSave,View.OnClickListener{

    EditText etPartyName,etVehicleNo;
    TextView tvDateandTime;
    Button bQR,bSave;
    ImageView ivQRCode;
    Gson gson = new Gson();
    SaveData saveData;
    Common common;
    Bundle B;
    int UserId;
    String UserName;

    String sPartyName = "";
    String sVehicle = "";
    String dDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrgeneration);
        findViewsById();
        B = getIntent().getExtras();
        UserId = B.getInt("userid");
        bQR.setOnClickListener(this);
        bSave.setOnClickListener(this);
    }
    private void findViewsById() {
        etPartyName = (EditText) findViewById(R.id.etPartyName);
        etVehicleNo = (EditText) findViewById(R.id.etVehicleNo);
        tvDateandTime = (TextView) findViewById(R.id.tvDate);
        bQR = (Button) findViewById(R.id.bQR);
        bSave = (Button) findViewById(R.id.bSave);
        bQR.setEnabled(false);
        bSave.setEnabled(false);
        ivQRCode = (ImageView) findViewById(R.id.ivQRCode);

        // show current Date and time in the TextView
        final Handler someHandler = new Handler(getMainLooper());
        someHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tvDateandTime.setText(new SimpleDateFormat("dd/MM/yyyy HH/mm/ss", Locale.getDefault()).format(new Date()));
                someHandler.postDelayed(this, 1000);
            }
        }, 10);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                bSave.setEnabled(false);
                if((etPartyName.getText().toString().length()>0) && (etVehicleNo.getText().toString().length()>0)){
                    bQR.setEnabled(true);
                }
//                Log.e("text",s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        etPartyName.addTextChangedListener(textWatcher);
        etVehicleNo.addTextChangedListener(textWatcher);

    }
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bQR:
                generateQR();
                bSave.setEnabled(true);
                break;
            case R.id.bSave:
                // save image to internal storage
/*                BitmapDrawable drawable = (BitmapDrawable) ivQRCode.getDrawable();
                Bitmap bitmap = drawable.getBitmap();

                File file;
                String path = Environment.getExternalStorageDirectory().toString();
                file = new File(path, "aaa"+".png");
                try{
                    ByteArrayOutputStream  stream = null;
                    stream = new ByteArrayOutputStream ();
                    bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
                    byte[] bitmapdata = stream.toByteArray();
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
                Uri savedImageURI = Uri.parse(file.getAbsolutePath());
                Toast.makeText(this,"Image saved in external storage.\n" + savedImageURI,Toast.LENGTH_SHORT).show();
*/
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);

/*
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                BitmapDrawable drawable = (BitmapDrawable) ivQRCode.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                // String encodedImage = Base64.encodeToString(baos.toByteArray(), 0);
                try {
                    Log.d("log", "file:" + File.createTempFile("JPEG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + "_", ".png", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)).getAbsolutePath());
                } catch (Exception ex) {
                    Log.d("log", ex.getLocalizedMessage());
                }
                MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "arun", "arunpics");
*/
/*                ContextWrapper wrapper = new ContextWrapper(getApplicationContext());
                File directory = wrapper.getDir("Images",MODE_PRIVATE);
                Log.e("directory",directory.toString());
                //file = new File(file, tvDateandTime.getText().toString()+".jpg");
                File file = new File(directory, "a.jpg");
                try{
                    FileOutputStream stream = null;
                    stream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);
                    stream.flush();
                    stream.close();
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
                Uri savedImageURI = Uri.parse(file.getAbsolutePath());
                Toast.makeText(this,"Image Saved to Internal Storage",Toast.LENGTH_SHORT).show();
*/


                saveData = new SaveData();
                saveData.delegate=this;
                saveData.execute();
                bQR.setEnabled(false);
                ivQRCode.setImageBitmap(null);
                etPartyName.setText("");
                etVehicleNo.setText("");
                bSave.setEnabled(false);
                break;
        }
    }

    public void processFinish(String output){
        //this you will received result fired from async class of onPostExecute(result) method.
        Log.e("result",output);
    }

    public void generateQR(){
        sPartyName = etPartyName.getText().toString();
        sVehicle = etVehicleNo.getText().toString();
        dDate = tvDateandTime.getText().toString();
        String fullString = "partyname:"+sPartyName+","+"vehicle:"+sVehicle+","+"date:"+dDate;
        String serializedString = gson.toJson(fullString);
        Bitmap bitmap = QRCodeHelper
                .newInstance(this)
                .setContent(serializedString)
                .setErrorCorrectionLevel(ErrorCorrectionLevel.L)
                .setMargin(2)
                .getQRCOde();
        ivQRCode.setImageBitmap(bitmap);
    }

    private class SaveData extends AsyncTask<String, String, String> {
        public AsyncResponseSave delegate=null;

        @Override
        protected void onPostExecute(String result) {
            delegate.processFinish(result);
            Log.e("asnyc result",result);
        }

        protected String doInBackground(String... args) {
            ArrayList<HashMap<String, Object>> MachList = new ArrayList<HashMap<String, Object>>();
            Connection con =  null;
            PreparedStatement ps = null;
            try {
                con = MysqlConnection.getConnection();
                Log.e("conect",con.toString());
                String sql2 =  "insert into tbl_qrcode_details(PartyName,VehicleNo,EntryDate,userid) values(?,?,?,?) ";
                ps = con.prepareStatement(sql2 );
                ps.setString(1,sPartyName);
                ps.setString(2,sVehicle);
                Log.e("date 1",""+dDate );
                DateFormat formatter=new SimpleDateFormat("dd/MM/yyyy HH/mm/ss");
                Date date = formatter.parse(dDate);
                java.sql.Timestamp sqlDate = new java.sql.Timestamp(date.getTime());
                Log.e("date 2",""+sqlDate );
                ps.setTimestamp (3,sqlDate );
                ps.setInt(4,UserId);
                ps.execute();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            finally {
                try
                { ps.close();con.close();} catch (Exception ex){ex.printStackTrace();}
            }
            return "DATA SAVED";
        }
    }


}
