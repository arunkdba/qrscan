package amarjothi.in.qrscan.QR;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import amarjothi.in.qrscan.DBManagement.DBManager;
import amarjothi.in.qrscan.Login;
import amarjothi.in.qrscan.R;
import amarjothi.in.qrscan.dbconnection.MysqlConnection;
import amarjothi.in.qrscan.dbconnection.OracleDBConnection;
import amarjothi.in.qrscan.interfaces.AsyncResponseSave;
import amarjothi.in.qrscan.models.CountTypeModel;
import amarjothi.in.qrscan.models.MachineDataModel;
import amarjothi.in.qrscan.models.ProcessTypeModel;
import amarjothi.in.qrscan.models.QRDataModel;
import amarjothi.in.qrscan.models.TubeDataModel;
import amarjothi.in.qrscan.models.YarnTypeModel;
import amarjothi.in.qrscan.util.Common;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import static android.graphics.Color.WHITE;

public class QRView extends AppCompatActivity implements ZXingScannerView.ResultHandler, View.OnClickListener, AsyncResponseSave {
    // Declare Barcode Scanner
    // dependance added in build.gradle
    me.dm7.barcodescanner.zxing.ZXingScannerView qrCodeScanner;
    SaveData saveData;
    private String HUAWEI = "huawei";
    private int MY_CAMERA_REQUEST_CODE = 6515;
    Common common = new Common();
    TextView tvDetails, tvUserName;
    Button bSave, bLogout;
    Bundle B;
    int UserId;
    String scanResult, UserName;
    int cameraposition;
    ArrayList<ProcessTypeModel> ProcessTypeList;
    ArrayList<MachineDataModel> MachineTypeList;

    // To store scanned QRCodes Data - Fetched from qrcode table and user entered values
    QRDataModel qrData;
    // Selected process type from the user interface
    ProcessTypeModel selectedProecssType;
    // Dyeing/Processing select Yes/NO from combo box. Saved to this variable
    String DyeingProcessing;

    // Check local oracle db configured with sqllite
    boolean localoracledb;
    Connection con;
    Connection conLogin;

    private DBManager dbManager;

    ViewGroup ReverseView;
    AlertDialog alert1;

    int ResumeAndWindowsFocuscontrol =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Override error to access database from main thread.
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        dbManager = new DBManager(this);
        dbManager.open();


        B = getIntent().getExtras();
        UserId = B.getInt("userid");
        UserName = B.getString("username");
        cameraposition = B.getInt("cameraposition");
        localoracledb = B.getBoolean("localoracledb");
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvUserName.setText(B.getString("username"));
        ProcessTypeList = (ArrayList<ProcessTypeModel>) B.getSerializable("processtypelist");
        MachineTypeList = (ArrayList<MachineDataModel>) B.getSerializable("machinetypelist");

//        bSave = findViewById(R.id.bSave);
        bLogout = findViewById(R.id.bLogout);
//        bSave.setOnClickListener(this);
        bLogout.setOnClickListener(this);
        // Check no of camera in the device and get details
        CameraManager cameraManager = (CameraManager) getSystemService(CAMERA_SERVICE);
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
//                Log.e("camera",""+facing);
                if (facing != null && facing.equals(CameraCharacteristics.LENS_FACING_FRONT)) {
//                    Log.e("camera",""+facing);
                }
                // Do something with the characteristics
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        // added at content_main.xml
        qrCodeScanner = findViewById(R.id.qrCodeImage);
        tvDetails = findViewById(R.id.tvDetails);
        // set Properties for scanner
        setScannerProperties();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            Log.e("qrview destroy", "onDestry");
/*            if(con != null){
                con.close();
                Log.e("qrview destroy","con closed");
            }

 */
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.bLogout):
                Intent QRLogin = new Intent(QRView.this, Login.class);
                QRLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(QRLogin);
                break;
        }

    }

    public void processFinish(String output) {
        //this you will received result fired from async class of onPostExecute(result) method.
        Log.e("result", output);
        if (output.equals("DATA SAVED")) {
            common.createCustomToast(getApplicationContext(), "Data Saved", (ViewGroup) findViewById(R.id.toast_layout_root), R.drawable.man_50);
        } else {
            common.createCustomToast(getApplicationContext(), "Data Not Saved " + output, (ViewGroup) findViewById(R.id.toast_layout_root), R.drawable.man_50);
        }
        qrCodeScanner.resumeCameraPreview(this);
        tvDetails.setText("");
    }


    @Override
    // Call Back method
    public void handleResult(Result rawResult) {
        // Result give details about scanned barcode
        if (rawResult != null) {
//            Toast.makeText(this,rawResult.getText(),Toast.LENGTH_LONG).show();
            tvDetails.setText(rawResult.getText().toString());
            scanResult = rawResult.getText().toString();
//            common.createCustomToast(getApplicationContext(),rawResult.getText(), (ViewGroup)findViewById(R.id.toast_layout_root),R.drawable.man_50);
            Log.e("Result 1", rawResult.getText()); // Prints scan results
            Log.e("Result 2", rawResult.getBarcodeFormat().getClass().toString());
            // Get Data from database based on the scanned QRCode
            QRDataModel qrData = getData(rawResult.getText());
            if (qrData != null) {
                ShowDialog(qrData);
                qrCodeScanner.stopCamera();
            } else {
                qrCodeScanner.resumeCameraPreview(this);
                tvDetails.setText("");
                Toast.makeText(this, "No Data Found", Toast.LENGTH_LONG).show();
            }
        }
    }

    // Get details of the scanned QRCode
    private QRDataModel getData(String scannedQRData) {
        qrData = new QRDataModel();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            if (localoracledb) {
//                con = getOracleConnection();
                con = OracleDBConnection.getConnection("dummy");
            } else {
                con = MysqlConnection.getConnection();
            }

            String sql2 = " select batchno,filcust,colourid,filcounts,mcname,mctype,labdipno,btn,totbqty," +
                    " qrcodedata,jobcemid,partyid,macno,colour from  " +
                    " tbl_qrcode_data where qrcodedata= ?";
            ps = con.prepareStatement(sql2);
            ps.setString(1, scannedQRData);
            rs = ps.executeQuery();
            // If scanned QR Data not found in the DB
            if (!rs.isBeforeFirst()) {
                return null;
            }
            // If scanned QR Data  found in the DB
            while (rs.next()) {
                Log.e("data", rs.getString(5));
                qrData.setBatcno(rs.getString(1));
                qrData.setFilcust(rs.getString(2));
                qrData.setColourid(rs.getDouble(3));
                qrData.setFilcounts(rs.getString(4));
                qrData.setMcname(rs.getString(5));
                qrData.setMctype(rs.getString(6));
                qrData.setLabdipno(rs.getString(7));
                qrData.setBtn(rs.getString(8));
                qrData.setTotbqty(rs.getString(9));
                qrData.setQrcodedata(rs.getString(10));
                qrData.setJobcemid(rs.getString(11));
                qrData.setPartyid(rs.getDouble(12));
                qrData.setMacno(rs.getDouble(13));
                qrData.setColor(rs.getString(14));
                Log.e("qrdata mc", qrData.getMcname());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return qrData;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ResumeAndWindowsFocuscontrol = 1;
        Log.e("focus", "qrview resume");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Checks permission for using camera
            // if Not asks for Permission
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                String[] st = new String[]{Manifest.permission.CAMERA};
                ActivityCompat.requestPermissions(this, st, MY_CAMERA_REQUEST_CODE);
                return;
            }
        }
        // Camera Open here
        // 1 - Opens front Camera
        // qrCodeScanner.startCamera(0);
        qrCodeScanner.startCamera(cameraposition);
        // Handled binded to scanner
        // if any scans happens result send to callback method handleResult()
        qrCodeScanner.setResultHandler(this);
    }

    // If dialog box cancelled . This Life cycle hook executed
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Log.e("focus windows", ""+hasFocus);
        //##arun##
        if (ResumeAndWindowsFocuscontrol != 1  && hasFocus == true) {
            qrCodeScanner.startCamera(cameraposition);
            qrCodeScanner.setResultHandler(this);
        }
        ResumeAndWindowsFocuscontrol = 0;

    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeScanner.stopCamera();
    }

    private void setScannerProperties() {
        //qrCodeScanner.setFormats(listOf(BarcodeFormat.QR_CODE));
        ArrayList<BarcodeFormat> ar = new ArrayList();
        // ZXing ("zebra crossing") is an open-source, multi-format 1D/2D barcode image processing library implemented in Java
        // Dependance added
        // com.google.zxing.BarcodeFormat
        ar.add(BarcodeFormat.QR_CODE);
        qrCodeScanner.setFormats(ar);
        qrCodeScanner.setAutoFocus(true);
        qrCodeScanner.setLaserColor(R.color.colorAccent);
        qrCodeScanner.setMaskColor(R.color.colorAccent);
        if (Build.MANUFACTURER.equals("HUAWEI"))
            qrCodeScanner.setAspectTolerance(0.5f);

    }

    private class SaveData extends AsyncTask<Object, String, String> {
        View view;

        SaveData(Object object) {
            view = (View) object;
        }

        public AsyncResponseSave delegate = null;
        String partyname = "", VehicleNo = "", EntryDate = "";
        String JobNumber = "", Color = "", Process = "", ProcessOption = "", Operator = "", Incharge = "";
        double Bat_qty = 0, Comp_qty = 0, Bal_qty = 0, Prod_qty = 0;
        int no_Cheese = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
/*
            // Data received format :: Job Number:10,Color:Red,Customer:The Company Name,Process:Processname,Process Option:End,Bat Qty:10,Al.Comp Qty:20,Bal Qty:30,Prod Qty:30,No Cheese:2,Operator:Operator Name,Incharge:Incharge Name
            // Data received format :: 06.08.2020  "Single string" can be used as a code
            // and get all data from Select * from tbl_qrcode_data based on this resultance string
            View v = view.findViewById(R.id.tvProdQtyData);
            Log.e("data", "" + ((RadioButton) view.findViewById(R.id.rbStart)).isChecked());
            Prod_qty = Double.parseDouble(((TextView) v).getText().toString());
            if (((RadioButton) view.findViewById(R.id.rbEnd)).isChecked()) {
                ProcessOption = "End";
            } else {
                ProcessOption = "Start";
            }
            String[] scannedData = scanResult.split(",");
            for (String st : scannedData) {
                if (st.split(":")[0].equals("Job Number")) {
                    JobNumber = st.split(":")[1];
                } else if (st.split(":")[0].equals("Color")) {
                    Color = st.split(":")[1];
                } else if (st.split(":")[0].equals("Customer")) {
                    partyname = st.split(":")[1];
                } else if (st.split(":")[0].equals("Process")) {
                    Process = st.split(":")[1];
                } else if (st.split(":")[0].equals("Bat Qty")) {
                    Bat_qty = Double.parseDouble(st.split(":")[1]);
                } else if (st.split(":")[0].equals("Al.Comp Qty")) {
                    Comp_qty = Double.parseDouble(st.split(":")[1]);
                } else if (st.split(":")[0].equals("Bal Qty")) {
                    Bal_qty = Double.parseDouble(st.split(":")[1]);
                } else if (st.split(":")[0].equals("No Cheese")) {
                    Process = st.split(":")[1];
                } else if (st.split(":")[0].equals("Operator")) {
                    Operator = st.split(":")[1];
                } else if (st.split(":")[0].equals("Incharge")) {
                    Incharge = st.split(":")[1];
                }
            }
 */
        }

        @Override
        protected void onPostExecute(String result) {
            delegate.processFinish(result);
            Log.e("asnyc result", result);
        }

        protected String doInBackground(Object... args) {
            ArrayList<HashMap<String, Object>> MachList = new ArrayList<HashMap<String, Object>>();
            PreparedStatement ps = null;

            try {
                if (localoracledb) {
                    // con = getOracleConnection();
                    con = OracleDBConnection.getConnection("dummy");
                } else {
                    con = MysqlConnection.getConnection();
                }

                Log.e("conect", con.toString());
                String sql2;
                if (localoracledb) {
                    sql2 = "insert into HYDRY_DET(username,modifiedon,createdby,createdon,partyid,Color,Batchqty,alprodqty,Balqty,noofchecone,processname,macno,operator,incharge,prodttm,batchno,counts,stkqty,prodt,frerep,dyefin,hydry_detid,prodse)";
                    sql2 = sql2 + " values(?,sysdate,?,sysdate,? ,?,?,?,?,?,?,?,?,?,to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),?,?,?,sysdate,?,?,supplier_seq.nextval,?)";
                } else {
                    sql2 = "insert into HYDRY_DET(username,modifiedon,createdby,createdon,partyid,Color,Batchqty,alprodqty,Balqty,noofchecone,processname,macno,operator,incharge,prodttm,batchno,counts,stkqty,prodt,frerep,dyefin,prodse)";
                    sql2 = sql2 + " values(?,now(),?,now(),? ,?,?,?,?,?,?,?,?,?,now(),?,?,?,now(),?,?,?)";
                }

                ps = con.prepareStatement(sql2);
                ps.setString(1, UserName);
                ps.setString(2, UserName);
                ps.setDouble(3, qrData.getPartyid());
                ps.setDouble(4, qrData.getColourid());

                ps.setDouble(5, Double.parseDouble(((TextView) (view.findViewById(R.id.tvBatQtyData))).getText().toString()));
                ps.setDouble(6, Double.parseDouble(((TextView) (view.findViewById(R.id.tvProdQtyData))).getText().toString()));
                ps.setDouble(7, Double.parseDouble(((TextView) (view.findViewById(R.id.tvBatQtyData))).getText().toString()));
                Log.e("cheese ", "" + ((TextView) (view.findViewById(R.id.etCheeseData))).getText().toString().length());
                double cheeseData = 0;
                if (((TextView) (view.findViewById(R.id.etCheeseData))).getText().toString().length() == 0) {
                    cheeseData = 0;
                } else {
                    cheeseData = Double.parseDouble(((TextView) (view.findViewById(R.id.etCheeseData))).getText().toString());
                }
                ps.setDouble(8, cheeseData);
                //Process Name
                ps.setString(9, selectedProecssType.getProcessid());
                ps.setDouble(10, qrData.getMacno());
                ps.setString(11, ((TextView) (view.findViewById(R.id.etOperatorData))).getText().toString());
                ps.setString(12, ((TextView) (view.findViewById(R.id.etInchargeData))).getText().toString());
                ps.setDouble(13, Double.parseDouble(qrData.getJobcemid()));
                ps.setString(14, ((TextView) (view.findViewById(R.id.tvCountData))).getText().toString());
                ps.setDouble(15, Double.parseDouble(((TextView) (view.findViewById(R.id.tvStockQtyData))).getText().toString()));
                ps.setString(16, ((TextView) (view.findViewById(R.id.etFreshorReProcessData))).getText().toString());
                ps.setString(17, DyeingProcessing);

                Log.e("radio data ", "" + ((RadioButton) view.findViewById(R.id.rbStart)).isChecked());
                if (((RadioButton) view.findViewById(R.id.rbEnd)).isChecked()) {
                    ProcessOption = "End";
                } else {
                    ProcessOption = "Start";
                }
                ps.setString(18, ProcessOption);

                ps.execute();
            } catch (Exception ex) {
                ex.printStackTrace();
                return ex.getMessage();
                //return "DATA NOT SAVED";
            } finally {
                try {
                    ps.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return "DATA SAVED";
        }
    }

    public void ShowDialog(QRDataModel qrData) {
        LayoutInflater layoutInflater1 = LayoutInflater.from(this);
        ReverseView = (ViewGroup) layoutInflater1.inflate(R.layout.layout_scan_save, null);
        AlertDialog.Builder alertDialogbuilder1 = new AlertDialog.Builder(QRView.this);
        alertDialogbuilder1.setView(ReverseView);
        final TextView tvJobNo = ReverseView.findViewById(R.id.tvJobNo);

        alertDialogbuilder1.setCancelable(true);
        //  ReverseBaleID.setOnTouchListener(this);
        alertDialogbuilder1.setPositiveButton("SAVE & CONTINUE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.e("dialog", "clicked");
                double dProdutcionQty = Double.parseDouble(((TextView) ReverseView.findViewById(R.id.tvProdQtyData)).getText().toString());
                double dBalanceQty = Double.parseDouble(((TextView) ReverseView.findViewById(R.id.tvBalQtyData)).getText().toString());
                if( dProdutcionQty >= dBalanceQty){
                    Toast.makeText(getApplicationContext(),"Production >= Balance Qty - Correct Error",Toast.LENGTH_SHORT).show();
                }else {
                    Save(ReverseView);
                }
            }
        });

        alert1 = alertDialogbuilder1.create();
        // Fetched data shown in the dialog box
        // For QR Code will all information
        //formatScannedData(ReverseView);
        // For QR Code only with data
        formatScannedData(ReverseView, qrData);
        alert1.show();
        alert1.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

    }

    public void formatScannedData(View view, final QRDataModel qrData) {
        // Process selection spinner
        final Spinner spProcessData = view.findViewById(R.id.spProcessData);
        ArrayAdapter processAdapter = new ArrayAdapter(this, R.layout.spinner, ProcessTypeList);
        spProcessData.setAdapter(processAdapter);

        Log.e("selected process ", spProcessData.getSelectedItem().toString());

        //if (spProcessData.getSelectedItem().toString().equals("Cheese Winding")) {
        //    CheeseWeighment(spProcessData.getSelectedItem().toString());

        // Balance qty = Batch Qty data from QR code - Completed Qty (from Qry)
        final TextView tvBalQtyData = view.findViewById(R.id.tvBalQtyData);

        // Completed Qty text box
        // Updated based on Process selection and Start::End Proecss Selection
        final TextView tvComQtyData = view.findViewById(R.id.tvComQtyData);

        // Production quantity
        final EditText tvProdQtyData = view.findViewById(R.id.tvProdQtyData);


        // Stock Qty Based on Process type / Production stage and Batch No
        final TextView tvStockQtyData = view.findViewById(R.id.tvStockQtyData);


        final RadioGroup radioGroupStart = view.findViewById(R.id.rgroup);
        // Radio Group initial selected radio button Value
        final String RadioGroupSelectedValue = ((RadioButton) radioGroupStart.findViewById(radioGroupStart.getCheckedRadioButtonId())).getText().toString();
        Log.e("selected box", ((RadioButton) radioGroupStart.findViewById(radioGroupStart.getCheckedRadioButtonId())).getText().toString());
        double dcompletedQty = getCompletedQty(qrData.getBatcno(), spProcessData.getSelectedItem().toString(), RadioGroupSelectedValue);
        tvComQtyData.setText("" + dcompletedQty);

        double dBalanceQty = Double.parseDouble(qrData.getTotbqty()) - dcompletedQty;
        tvBalQtyData.setText("" + dBalanceQty);

        double dStockQty = getStockQty(qrData.getBatcno(), spProcessData.getSelectedItem().toString(), RadioGroupSelectedValue);
        tvStockQtyData.setText("" + dStockQty);

        // machine selection spinner
        // Machine Names added to spinner based on the selected Process
        final Spinner spMachines = view.findViewById(R.id.spProcess_machine);
        final String sBatchNo = qrData.getBatcno();

        // Check balance and production qty. if Prod Qty>= Balance qty . Button will be disabled
        enableSaveButton();

        spProcessData.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View spProcessview, int position, long id) {

                // Radio Group  selected radio button Value
                String sSelectedStartEnd = ((RadioButton) radioGroupStart.findViewById(radioGroupStart.getCheckedRadioButtonId())).getText().toString();
                double dcompletedQty = getCompletedQty(qrData.getBatcno(), spProcessData.getSelectedItem().toString(), sSelectedStartEnd);
                tvComQtyData.setText("" + dcompletedQty);
                double dBalanceQty = Double.parseDouble(qrData.getTotbqty()) - dcompletedQty;
                tvBalQtyData.setText("" + dBalanceQty);
                double dStockQty = getStockQty(qrData.getBatcno(), spProcessData.getSelectedItem().toString(), sSelectedStartEnd);
                tvStockQtyData.setText("" + dStockQty);

                selectedProecssType = (ProcessTypeModel) parent.getSelectedItem();

                if (selectedProecssType.getProcessname().equals("Cheese Winding")) {
                    // This view comes from formatScannedData
                    // 9999 - Grossweight  9998 - Pump wt spinner
                    RemovePackingWeighment();
                    CheeseWeighment(spProcessData.getSelectedItem().toString());
                    UpdateProdQty(9999, 9998, spProcessData.getSelectedItem().toString());

                } else if (selectedProecssType.getProcessname().equals("Packing")) {
                    // This view comes from formatScannedData
                    // 9982 - Grossweight  9984 - Pump wt spinner
                    RemoveCheeseWeighment();
                    PackingWeighment(qrData.getBatchno(), spProcessData.getSelectedItem().toString());
                    UpdateProdQty(9982, 9984, spProcessData.getSelectedItem().toString());
                } else {
                    Log.e("remove", spProcessData.getSelectedItem().toString());
                    RemoveCheeseWeighment();
                    RemovePackingWeighment();
                    ((EditText) ReverseView.findViewById(R.id.etCheeseData)).setText("0");
                    ((EditText) ReverseView.findViewById(R.id.tvProdQtyData)).setText("" + dBalanceQty);
                }

                Log.e("proces type", selectedProecssType.getProcessname() + " " + selectedProecssType.getProcessid() + " " + selectedProecssType.getBatchnoflag());  //cheesewiding 10149000001062

                // Create machine array based on the process type
                ArrayList<MachineDataModel> tempMachineTypeList = new ArrayList();
                if (spProcessData.getSelectedItem().toString().equals("Yarn Dyeing") || spProcessData.getSelectedItem().toString().equals("Grey Issue")) {
                    tempMachineTypeList = updateMachineSpinner(selectedProecssType.getProcessname());
                } else {
                    for (MachineDataModel machine : MachineTypeList) {
                        if (machine.getGrpname() == null)
                            continue;
                        //Log.e("machine proces type", machine.getMachineid() + " " + machine.getProcessprocessid() + " " + selectedProecssType.getProcessid());
                        if (machine.getGrpname().equals("Cheese Winding") && spProcessData.getSelectedItem().toString().equals("Cheese Winding")) {
                            tempMachineTypeList.add(machine);
                        } else if (machine.getGrpname().equals("Hydro") && spProcessData.getSelectedItem().toString().equals("HYDRO")) {
                            tempMachineTypeList.add(machine);
                        } else if (machine.getGrpname().equals("RF Drier") && spProcessData.getSelectedItem().toString().equals("RF DRIER")) {
                            tempMachineTypeList.add(machine);
                        } else if (machine.getGrpname().equals("Yarn Dyeing") && spProcessData.getSelectedItem().toString().equals("Yarn Finishing")) {
                            tempMachineTypeList.add(machine);
                        } else if (machine.getGrpname().equals("Packing") && spProcessData.getSelectedItem().toString().equals("Packing")) {
                            tempMachineTypeList.add(machine);
                        }
                    }
                }
                // Here spMachine spinner updated with array based on the selected proecss
                if (tempMachineTypeList.size() > 0) {
                    Log.e("proces type", " " + tempMachineTypeList.get(0).getMachineid());
                    ArrayAdapter machineAdapter = new ArrayAdapter(getApplicationContext(), R.layout.spinner, tempMachineTypeList);
                    spMachines.setAdapter(machineAdapter);
                } else {
                    spMachines.setAdapter(null);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

            private ArrayList<MachineDataModel> updateMachineSpinner(String ProcessName) {
                StringBuilder sMachine = new StringBuilder("select  MACHINMASTID, MachineId,mcno  from Machinmast inner join jobcem ");
                sMachine.append(" on  jobcem.MCNO=Machinmast.MACHINMASTID ");
                //sMachine.append(" and Machinmast.grpname='" + ProcessName + "' and BATCHNO = '" + sBatchNo + "' ");
                sMachine.append(" and BATCHNO = '" + sBatchNo + "' ");
                ResultSet res = null;
                PreparedStatement ps = null;
                ArrayList<MachineDataModel> tempMachineTypeList = new ArrayList();

                try {
                    if (localoracledb) {
                        conLogin = OracleDBConnection.getConnection("dummy");
                    } else {
                        conLogin = MysqlConnection.getConnection();
                    }
                    ps = conLogin.prepareStatement(sMachine.toString());
                    res = ps.executeQuery();
                    while (res.next()) {
                        MachineDataModel machineModel = new MachineDataModel();
                        machineModel.setMachinmastid(res.getString(1));
                        machineModel.setMachineid(res.getString(2));   // Machine Name
                        tempMachineTypeList.add(machineModel);
                    }
                    return tempMachineTypeList;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }
        });

        Log.e("cust name", qrData.getFilcust());
        Log.e("cust name", qrData.getMcname());
        Log.e("mahine name", qrData.getMcname());
        //TextView tvMachineNo = view.findViewById(R.id.spProcess_machine);
        //tvMachineNo.setText(qrData.getMcname());
        TextView tvJobNo = view.findViewById(R.id.tvJobNoData);
        tvJobNo.setText(qrData.getBatcno());
        TextView tvColorData = view.findViewById(R.id.tvColorData);
        tvColorData.setText(qrData.getColor());
        TextView tvCustomerData = view.findViewById(R.id.tvCustomerData);
        tvCustomerData.setText(qrData.getFilcust());
        TextView tvCountData = view.findViewById(R.id.tvCountData);
        tvCountData.setText(qrData.getFilcounts());

        TextView tvBatQtyData = view.findViewById(R.id.tvBatQtyData);
        tvBatQtyData.setText(qrData.getTotbqty());

        final String[] DyeingFinishing = {"YES", "NO"};
        Spinner spDyeingProcessing = view.findViewById(R.id.spDyeingData);
        ArrayAdapter dyeingAdapter = new ArrayAdapter(this, R.layout.spinner, DyeingFinishing);
        dyeingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDyeingProcessing.setAdapter(dyeingAdapter);
        spDyeingProcessing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View spDyeingview, int position, long id) {
                DyeingProcessing = DyeingFinishing[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Get completed quantity based on process type , Batch No and Start/End of the Lot
        if (radioGroupStart != null) {
            radioGroupStart.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                String startorend = "";

                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    switch (i) {
                        case R.id.rbStart:
                            startorend = "Start";
                            double dcompletedQty = getCompletedQty(qrData.getBatcno(), spProcessData.getSelectedItem().toString(), startorend);
                            tvComQtyData.setText("" + dcompletedQty);
                            double dBalanceQty = Double.parseDouble(qrData.getTotbqty()) - dcompletedQty;
                            tvBalQtyData.setText("" + dBalanceQty);
                            double dStockQty = getStockQty(qrData.getBatcno(), spProcessData.getSelectedItem().toString(), startorend);
                            tvStockQtyData.setText("" + dStockQty);
                            enableSaveButton();
                            break;
                        case R.id.rbEnd:
                            startorend = "End";
                            double dcompletedQty1 = getCompletedQty(qrData.getBatcno(), spProcessData.getSelectedItem().toString(), startorend);
                            tvComQtyData.setText("" + dcompletedQty1);
                            double dBalanceQty1 = Double.parseDouble(qrData.getTotbqty()) - dcompletedQty1;
                            tvBalQtyData.setText("" + dBalanceQty1);
                            double dStockQty1 = getStockQty(qrData.getBatcno(), spProcessData.getSelectedItem().toString(), startorend);
                            tvStockQtyData.setText("" + dStockQty1);
                            enableSaveButton();
                            break;
                    }
                }
            });
        }

        EditText tvCheeseData = view.findViewById(R.id.etCheeseData);
        tvCheeseData.setText("");
        EditText tvOperatorData = view.findViewById(R.id.etOperatorData);
        tvOperatorData.setText("");
        EditText tvInchargeData = view.findViewById(R.id.etInchargeData);
        tvInchargeData.setText("");

        ((EditText) ReverseView.findViewById(R.id.tvProdQtyData)).addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!(((EditText) ReverseView.findViewById(R.id.tvProdQtyData)).getText().toString().equals(""))) {
                    enableSaveButton();
                }
            }

            public void afterTextChanged(Editable s) {
            }
        });


    }

    // Production quantity updation method based on Bag weight , Pumpwt and No of cheese
    private void UpdateProdQty(int viewidGrosswt, int viewIdPumpwtSpinner, String ProcessType) {
        double dPumpWeight = Double.parseDouble(((Spinner) ReverseView.findViewById(viewIdPumpwtSpinner)).getSelectedItem().toString());
        double dGrossweight = Double.parseDouble(((TextView) ReverseView.findViewById(viewidGrosswt)).getText().toString());
        int iNoofCheese = Integer.parseInt(((TextView) ReverseView.findViewById(R.id.etCheeseData)).getText().toString());
        if (ProcessType.equals("Cheese Winding")) {
            ((TextView) ReverseView.findViewById(R.id.tvProdQtyData)).setText("" + String.format("%.1f", (dGrossweight - (iNoofCheese * dPumpWeight))));
        } else if (ProcessType.equals("Packing")) {
            ((TextView) ReverseView.findViewById(R.id.tvProdQtyData)).setText("" + String.format("%.1f", (dGrossweight - (iNoofCheese * 0.05 )-dPumpWeight)));
        }
        enableSaveButton();
    }

    public double getCompletedQty(String BatchNo, String ProcessType, String ProcessStartOrEnd) {
        StringBuilder sCompletedQty = new StringBuilder("select sum(prodqty) totprodqty from hydry_det ");
        sCompletedQty.append(" inner join ProcessMast on hydry_det.Processname=  ProcessMast.PROCESSMASTID ");
        sCompletedQty.append(" inner join jobcem on jobcem.batchno= '" + BatchNo + "' ");
        sCompletedQty.append(" and ProcessMast.processid='" + ProcessType + "'  and hydry_det.prodse='" + ProcessStartOrEnd + "'");
        ResultSet res = null;
        PreparedStatement ps = null;
        Log.e("qry |", BatchNo + " |" + ProcessType + "| " + ProcessStartOrEnd + "|");
        //      Log.e("query ",sCompletedQty.toString());
        try {
            if (localoracledb) {
                conLogin = OracleDBConnection.getConnection("dummy");
            } else {
                conLogin = MysqlConnection.getConnection();
            }
            ps = conLogin.prepareStatement(sCompletedQty.toString());
            res = ps.executeQuery();
            res.next();
            return res.getDouble(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    private void enableSaveButton(){
        double dProdutcionQty = 0;
        double dBalanceQty = 0;
        if (!(((TextView) ReverseView.findViewById(R.id.tvProdQtyData)).getText().toString().equals("")) && !(((TextView) ReverseView.findViewById(R.id.tvBalQtyData)).getText().toString().equals(""))){
            dProdutcionQty = Double.parseDouble(((TextView) ReverseView.findViewById(R.id.tvProdQtyData)).getText().toString());
            dBalanceQty = Double.parseDouble(((TextView) ReverseView.findViewById(R.id.tvBalQtyData)).getText().toString());
        }else{
            return;
        }
        if( dProdutcionQty < dBalanceQty){
            alert1.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
        }else{
            alert1.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
        }
    }

    public double getStockQty(String BatchNo, String ProcessType, String ProcessStartOrEnd) {
        StringBuilder sstockQty = new StringBuilder(" select max(stkqty) stkqty from ( ");
        sstockQty.append(" select sum(A.BALQTY) stkqty from YRNTEMPSTK a where a.batchno='" + BatchNo + "' and ");
        sstockQty.append(" 'Grey Issue'='" + ProcessType + "' and 'End'='" + ProcessStartOrEnd + "'  ");
        sstockQty.append(" and a.stage='C. BATCH STOCK' ");
        sstockQty.append(" union all ");
        sstockQty.append(" select sum(A.BALQTY) stkqty from YRNTEMPSTK a where a.batchno='" + BatchNo + "' and  ");
        sstockQty.append(" 'Cheese Winding'='" + ProcessType + "' and 'End'='" + ProcessStartOrEnd + "'  ");
        sstockQty.append(" and a.stage='D. IN CHEESE WINDING' ");
        sstockQty.append(" union all ");
        sstockQty.append(" select sum(A.BALQTY) stkqty from YRNTEMPSTK a where a.batchno='" + BatchNo + "'  ");
        sstockQty.append(" and 'Yarn Dyeing'='" + ProcessType + "' and 'Start'='" + ProcessStartOrEnd + "'  ");
        sstockQty.append(" and a.stage='E. READY FOR COLORING' ");
        sstockQty.append(" union all ");
        sstockQty.append(" select sum(A.BALQTY) stkqty from YRNTEMPSTK a where a.batchno='" + BatchNo + "'  ");
        sstockQty.append(" and 'Yarn Dyeing'='" + ProcessType + "' and 'End'='" + ProcessStartOrEnd + "'  ");
        sstockQty.append(" and a.stage='F. IN COLORING' ");
        sstockQty.append(" union all ");
        sstockQty.append(" select sum(A.BALQTY) stkqty from YRNTEMPSTK a where a.batchno='" + BatchNo + "'  ");
        sstockQty.append(" and 'Yarn Finishing'='" + ProcessType + "' and 'End'='" + ProcessStartOrEnd + "'  ");
        sstockQty.append(" and a.stage='G3. UNDER FINISHING' ");
        sstockQty.append(" union all ");
        sstockQty.append(" select sum(A.BALQTY) stkqty from YRNTEMPSTK a where a.batchno='" + BatchNo + "' and  ");
        sstockQty.append(" 'HYDRO'='" + ProcessType + "' and 'End'='" + ProcessStartOrEnd + "' and a.stage='H. IN HYDRO' ");
        sstockQty.append(" union all ");
        sstockQty.append(" select sum(A.BALQTY) stkqty from YRNTEMPSTK a where a.batchno='" + BatchNo + "'  ");
        sstockQty.append(" and 'RF DRIER'='" + ProcessType + "' and 'End'='" + ProcessStartOrEnd + "' and a.stage='I. IN DRIER' ");
        sstockQty.append(" union all ");
        sstockQty.append(" select sum(A.BALQTY) stkqty from YRNTEMPSTK a where a.batchno='" + BatchNo + "'  ");
        sstockQty.append(" and 'Cone Winding'='" + ProcessType + "' and 'End'='" + ProcessStartOrEnd + "'  ");
        sstockQty.append(" and a.stage='J. IN CONE WINDING' ");
        sstockQty.append(" union all ");
        sstockQty.append(" select sum(A.BALQTY) stkqty from YRNTEMPSTK a where a.batchno='" + BatchNo + "'  ");
        sstockQty.append(" and 'Packing'='" + ProcessType + "' and 'End'='" + ProcessStartOrEnd + "'  ");
        sstockQty.append(" and a.stage='K. READY FOR PACKING' ");
        sstockQty.append(" ) a ");


        ResultSet res = null;
        PreparedStatement ps = null;
        Log.e("qry |", BatchNo + " |" + ProcessType + "| " + ProcessStartOrEnd + "|");
//        Log.e("query ",sstockQty.toString());
        try {
            if (localoracledb) {
                conLogin = OracleDBConnection.getConnection("dummy");
            } else {
                conLogin = MysqlConnection.getConnection();
            }
            ps = conLogin.prepareStatement(sstockQty.toString());
            res = ps.executeQuery();
            res.next();
            return res.getDouble(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public void RemoveCheeseWeighment() {
        ReverseView.removeView(ReverseView.findViewById(999));
    }

    public void RemovePackingWeighment() {
        ReverseView.removeView(ReverseView.findViewById(999));
        ReverseView.removeView(ReverseView.findViewById(998));
    }

    // Used for Cheese Winding Processing
    // Show Tube wet / Set 20 cheese default
    public void CheeseWeighment(final String ProcessType) {
        // Get Tube weight based on process Type
        ArrayList<TubeDataModel> TubeWegithList = getTubeWeight(ProcessType);
        ((TextView) ReverseView.findViewById(R.id.etCheeseData)).setText("20");

        //Paraent Layout
        LinearLayout layoutparent = (LinearLayout) findViewById(R.id.fulllayout);
        View layoutCheese = ReverseView.findViewById(R.id.layoutCheese);

        int indexLayoutCheese = ((ViewGroup) ReverseView).indexOfChild(layoutCheese);
        Log.e("index", "" + indexLayoutCheese + " " + layoutCheese);

        // New Linear Layout for GRMS and Weight reading
        LinearLayout layout2 = new LinearLayout(getApplicationContext());
        layout2.setId(999);
        layout2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout2.setOrientation(LinearLayout.HORIZONTAL);
        layout2.setWeightSum(2);
        EditText ETWeight;
        Spinner SPCheeseWeight;
        ETWeight = new EditText(this);
        SPCheeseWeight = new Spinner(this);
        ETWeight.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
        SPCheeseWeight.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
        SPCheeseWeight.setBackground(getDrawable(R.drawable.spinnerborder));
        SPCheeseWeight.setId(9998);

        ETWeight.setBackground(getDrawable(R.drawable.textborder));
        ETWeight.setTextColor(WHITE);
        ETWeight.setHint("Gross Weight");
        ETWeight.setText("0");
        ETWeight.setId(9999);
        SPCheeseWeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View spDyeingview, int position, long id) {
                // Production quantity updation method based on Bag weight , Pumpwt and No of cheese
                UpdateProdQty(9999, 9998,ProcessType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayAdapter TubeWeightAdapter = new ArrayAdapter(this, R.layout.spinner, TubeWegithList);
        SPCheeseWeight.setAdapter(TubeWeightAdapter);

        layout2.addView(SPCheeseWeight);
        layout2.addView(ETWeight);
        ((ViewGroup) ReverseView).addView(layout2, indexLayoutCheese);
        //Pass Weighment text box id and Pump weight spinner Id  to enable Text watcher
        addTextWatcher(9999, 9998);
    }

    // Used for Cheese Winding Processing
    // Show Tube wet / Set 20 cheese default
    public void PackingWeighment(String BatchNo, final String ProcessType) {
        // Get Tube weight based on process Type
        ArrayList<TubeDataModel> TubeWegithList = getTubeWeight(ProcessType);

        // Get Count for the batch No
        ArrayList<CountTypeModel> CountList = getCountType(BatchNo);
        // Get Yarn type based on the First count Type and Batch No
        ArrayList<YarnTypeModel> YarnTypeList = getYarnType(CountList.get(0).getCountsId(), BatchNo);
        Log.e("countlist", CountList.toString());
        ((TextView) ReverseView.findViewById(R.id.etCheeseData)).setText("40");

        View layoutCheese = ReverseView.findViewById(R.id.layoutCheese);

        int indexLayoutCheese = ((ViewGroup) ReverseView).indexOfChild(layoutCheese);
        Log.e("index", "" + indexLayoutCheese + " " + layoutCheese);

        // New Linear Layout for GRMS and Weight reading
        LinearLayout layout2 = new LinearLayout(getApplicationContext());
        layout2.setId(999);
        layout2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout2.setOrientation(LinearLayout.HORIZONTAL);
        layout2.setWeightSum(2);

        LinearLayout layout3 = new LinearLayout(getApplicationContext());
        layout3.setId(998);
        layout3.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout3.setOrientation(LinearLayout.HORIZONTAL);
        layout3.setWeightSum(3);

        EditText ETWeight;
        TextView TvBagNo;
        Spinner SPCount, SPYarnType;
        Spinner SPCheeseWeight;

        TvBagNo = new TextView(this);
        ETWeight = new EditText(this);
        SPCount = new Spinner(this);
        SPYarnType = new Spinner(this);
        SPCheeseWeight = new Spinner(this);
        ETWeight.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));

        SPCheeseWeight.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f));
        SPCheeseWeight.setBackground(getDrawable(R.drawable.spinnerborder));
        SPCheeseWeight.setId(9984);
        ETWeight.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f));
        TvBagNo.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f));

        ETWeight.setBackground(getDrawable(R.drawable.textborder));
        ETWeight.setTextColor(WHITE);
        ETWeight.setHint("Gross Weight");
        ETWeight.setText("0");
        ETWeight.setId(9982);
        TvBagNo.setBackground(getDrawable(R.drawable.labelborder));
        TvBagNo.setTextSize(20);
        TvBagNo.setText(getbagNo());
        TvBagNo.setId(9983);
        ArrayAdapter TubeWeightAdapter = new ArrayAdapter(this, R.layout.spinner, TubeWegithList);
        SPCheeseWeight.setAdapter(TubeWeightAdapter);


        layout3.addView(TvBagNo);
        layout3.addView(SPCheeseWeight);
        layout3.addView(ETWeight);

        SPCount.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
        SPCount.setBackground(getDrawable(R.drawable.spinnerborder));
        SPCount.setId(9980);
        SPYarnType.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
        SPYarnType.setBackground(getDrawable(R.drawable.spinnerborder));
        SPYarnType.setId(9981);

        ArrayAdapter CountAdapter = new ArrayAdapter(this, R.layout.spinner, CountList);
        SPCount.setAdapter(CountAdapter);
        layout2.addView(SPCount);
        ArrayAdapter YarnTypeAdapter = new ArrayAdapter(this, R.layout.spinner, YarnTypeList);
        SPYarnType.setAdapter(YarnTypeAdapter);
        layout2.addView(SPYarnType);
        SPCheeseWeight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View spDyeingview, int position, long id) {
                // Production quantity updation method based on Bag weight , Pumpwt and No of cheese
                UpdateProdQty(9982, 9984,ProcessType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        ((ViewGroup) ReverseView).addView(layout2, indexLayoutCheese);
        ((ViewGroup) ReverseView).addView(layout3, indexLayoutCheese + 1);

        //Pass Weighment text box id and Pump weight spinner Id   to enable Text watcher
        addTextWatcher(9982, 9984);
    }

    private void addTextWatcher(final int ViewIdWeight, final int ViewIdPumpWt) {
        // Watcher for Gross weight
        Log.e("textwater", "ViewIdWeight  " + ViewIdWeight + "ViewIdPumpWt  " + ViewIdPumpWt);
        ((EditText) ReverseView.findViewById(ViewIdWeight)).addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!(((EditText) ReverseView.findViewById(ViewIdWeight)).getText().toString().equals(""))) {
                    double dPumpWeight = Double.parseDouble(((Spinner) ReverseView.findViewById(ViewIdPumpWt)).getSelectedItem().toString());
                    double dGrossweight = Double.parseDouble(((TextView) ReverseView.findViewById(ViewIdWeight)).getText().toString());
                    int iNoofCheese = Integer.parseInt(((TextView) ReverseView.findViewById(R.id.etCheeseData)).getText().toString());
                    ((EditText) ReverseView.findViewById(R.id.tvProdQtyData)).setText("" + String.format("%.1f", (dGrossweight - (iNoofCheese * dPumpWeight))));
                }
            }

            public void afterTextChanged(Editable s) {
            }
        });
        // Watcher for No of Cheese
        ((TextView) ReverseView.findViewById(R.id.etCheeseData)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (ReverseView.findViewById(ViewIdWeight) != null && !(((EditText) ReverseView.findViewById(ViewIdWeight)).getText().toString().equals(""))) {
                    double dPumpWeight = Double.parseDouble(((Spinner) ReverseView.findViewById(ViewIdPumpWt)).getSelectedItem().toString());
                    double dGrossweight = Double.parseDouble(((TextView) ReverseView.findViewById(ViewIdWeight)).getText().toString());
                    int iNoofCheese = Integer.parseInt(((TextView) ReverseView.findViewById(R.id.etCheeseData)).getText().toString());
                    ((EditText) ReverseView.findViewById(R.id.tvProdQtyData)).setText("" + String.format("%.1f", (dGrossweight - (iNoofCheese * dPumpWeight))));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }



        public ArrayList<TubeDataModel> getTubeWeight(String ProcessType) {
        StringBuilder sCompletedQty = new StringBuilder("select a.TubeMastid, a.tubename,a.tubewt from TubeMast a,ProcessMast b  ");
        sCompletedQty.append(" where A.PROCESSNAME=B.PROCESSMASTID and  ");
        sCompletedQty.append(" B.PROCESSID='" + ProcessType + "' order by a.tubename ");
        ResultSet res = null;
        PreparedStatement ps = null;
        try {
            if (localoracledb) {
                conLogin = OracleDBConnection.getConnection("dummy");
            } else {
                conLogin = MysqlConnection.getConnection();
            }
            ArrayList<TubeDataModel> TubeWeightList = new ArrayList();
            ps = conLogin.prepareStatement(sCompletedQty.toString());
            res = ps.executeQuery();
            while (res.next()) {
                TubeDataModel TD = new TubeDataModel();
                TD.setTubemastid(res.getString(1));
                TD.setTubename(res.getString(2));
                TD.setTubewt(res.getDouble(3));
                TubeWeightList.add(TD);
            }
            return TubeWeightList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ArrayList<CountTypeModel> getCountType(String BatcNo) {
        StringBuilder sCountQry = new StringBuilder("select a.CountsMastid,a.CountsId from countsmast a,Jobcem b,jobcedet c  ");
        sCountQry.append("  where b.jobcemid=c.jobcemid and a.countsmastid=c.COUNT and ");
        sCountQry.append(" b.batchno='" + BatcNo + "' group by a.CountsMastid,a.CountsId order by 2   ");
        Log.e("qry", sCountQry.toString());
        ResultSet res = null;
        PreparedStatement ps = null;
        try {
            if (localoracledb) {
                conLogin = OracleDBConnection.getConnection("dummy");
            } else {
                conLogin = MysqlConnection.getConnection();
            }
            ArrayList<CountTypeModel> CountList = new ArrayList();
            ps = conLogin.prepareStatement(sCountQry.toString());
            res = ps.executeQuery();
            while (res.next()) {
                CountTypeModel TD = new CountTypeModel();
                TD.setCountsMastid(res.getString(1));
                TD.setCountsId(res.getString(2));
                CountList.add(TD);
            }
            return CountList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ArrayList<YarnTypeModel> getYarnType(String sCount, String BatchNo) {


        StringBuilder YarnQry = new StringBuilder(" select a.yarntypemastid,a.yarntype from yarntypemast a,Jobcem b,jobcedet c,countsmast d ");
        YarnQry.append("  where a.yarntypemastid=c.ytype and ");
        YarnQry.append(" b.jobcemid=c.jobcemid and d.countsmastid=c.COUNT and d.countsid='" + sCount + "' and b.batchno='" + BatchNo + "' group by a.yarntypemastid,a.yarntype order by 2   ");
        ResultSet res = null;
        PreparedStatement ps = null;
        try {
            if (localoracledb) {
                conLogin = OracleDBConnection.getConnection("dummy");
            } else {
                conLogin = MysqlConnection.getConnection();
            }
            ArrayList<YarnTypeModel> YarnList = new ArrayList();
            ps = conLogin.prepareStatement(YarnQry.toString());
            res = ps.executeQuery();
            while (res.next()) {
                YarnTypeModel TD = new YarnTypeModel();
                TD.setYarntypemastid(res.getString(1));
                TD.setYarntype(res.getString(2));
                YarnList.add(TD);
            }
            return YarnList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    // get Bag No for the current Bag in weighment
    // No should be updated in hydry_det after checking the bag no's uniqueness - arun.
    public String getbagNo() {
        StringBuilder BagQry = new StringBuilder(" select nvl(a.bagno,0)+1 bagno from ( ");
        BagQry.append("  select max(A.BAGNO) bagno from hydry_det a,processmast b where  ");
        BagQry.append(" A.PROCESSNAME=B.PROCESSMASTID and B.PROCESSID='Packing' ) a  ");
        ResultSet res = null;
        PreparedStatement ps = null;
        try {
            if (localoracledb) {
                conLogin = OracleDBConnection.getConnection("dummy");
            } else {
                conLogin = MysqlConnection.getConnection();
            }
            ps = conLogin.prepareStatement(BagQry.toString());
            res = ps.executeQuery();
            res.next();
            return "" + res.getInt(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /*
    public void formatScannedData(View view) {
        String partyname = "", VehicleNo = "", EntryDate = "", JobNumber = "";
        // Data received format :: Job Number:10,Color:Red,Customer:The Company Name,Process:Processname,Process Option:End,Bat Qty:10,Al.Comp Qty:20,Bal Qty:30,Prod Qty:30,No Cheese:2,Operator:Operator Name,Incharge:Incharge Name

        String[] scannedData = scanResult.split(",");
        for (String st : scannedData) {
            if (st.split(":")[0].equals("Job Number")) {
                Log.e("Job ", st.split(":")[1]);
                TextView tvJobNo = (TextView) view.findViewById(R.id.tvJobNoData);
                tvJobNo.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Color")) {
                TextView tvColorData = (TextView) view.findViewById(R.id.tvColorData);
                tvColorData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Customer")) {
                TextView tvCustomerData = (TextView) view.findViewById(R.id.tvCustomerData);
                tvCustomerData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Process")) {
                TextView tvProcessData = (TextView) view.findViewById(R.id.tvProcessData);
                tvProcessData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Bat Qty")) {
                TextView tvBatQtyData = (TextView) view.findViewById(R.id.tvBatQtyData);
                tvBatQtyData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Al.Comp Qty")) {
                TextView tvComQtyData = (TextView) view.findViewById(R.id.tvComQtyData);
                tvComQtyData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Bal Qty")) {
                TextView tvBalQtyData = (TextView) view.findViewById(R.id.tvBalQtyData);
                tvBalQtyData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Prod Qty")) {
                TextView tvProdQtyData = (TextView) view.findViewById(R.id.tvProdQtyData);
                tvProdQtyData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("No Cheese")) {
                TextView tvCheeseData = (TextView) view.findViewById(R.id.tvCheeseData);
                tvCheeseData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Operator")) {
                TextView tvOperatorData = (TextView) view.findViewById(R.id.tvOperatorData);
                tvOperatorData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Incharge")) {
                TextView tvInchargeData = (TextView) view.findViewById(R.id.tvInchargeData);
                tvInchargeData.setText(st.split(":")[1]);
            } else if (st.split(":")[0].equals("Process Option")) {
                RadioButton rbStart = (RadioButton) view.findViewById(R.id.rbStart);
                RadioButton rbEnd = (RadioButton) view.findViewById(R.id.rbEnd);
                if (st.split(":")[1].equals("End")) {
                    rbStart.setChecked(false);
                    rbEnd.setChecked(true);
                } else {
                    rbStart.setChecked(true);
                    rbEnd.setChecked(false);
                }
            }

        }

    }

     */
    public void Save(View view) {
        saveData = new SaveData(view);
        saveData.delegate = this;
        saveData.execute();
    }

    private Connection getOracleConnection() {
        Cursor cursor = dbManager.fetch();
        //public void insert(String ip, int port, String servicename, String username,String password) {
        Connection connection = null;
        String username =  cursor.getString(4);
        String dbHost =  cursor.getString(1); ;
        String dbName =  cursor.getString(3);;
        int ServerPort = cursor.getInt(2);;
        String password =  cursor.getString(5);;
        try {
            connection = OracleDBConnection.getConnection(dbHost, ServerPort, dbName, username, password);
            Log.e("connection ", connection.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return connection;
    }

}
