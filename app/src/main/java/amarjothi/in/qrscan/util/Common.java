package amarjothi.in.qrscan.util;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import amarjothi.in.qrscan.R;

/**
 * Created by admin on 1/12/2018.
 */

public class Common {
    public void createCustomToast(Context context, String sText,ViewGroup viewGroup,int resourceId){        // (Which Activity , Statement to display, Root Toast View , Image Icon )
        LayoutInflater  inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View layout = inflater.inflate(R.layout.toast_layout_root,
                        viewGroup);
        ImageView image = (ImageView) layout.findViewById(R.id.image);
        image.setImageResource(resourceId);
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(sText);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
}
