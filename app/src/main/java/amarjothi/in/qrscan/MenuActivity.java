package amarjothi.in.qrscan;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import amarjothi.in.qrscan.QR.QRGeneration;
import amarjothi.in.qrscan.QR.QRView;
import androidx.appcompat.app.AppCompatActivity;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener{
    Button bQRGeneration,bQRView,bAnnouncement,bActivity,bCalender;
    TextView tvStudentRegNo,tvUserName;
    String sStudentRegNo="";
    Bundle B;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu1);
//        tvStudentRegNo = (TextView)findViewById(R.id.tvStudentDet);
//        String sStudentDetails = getIntent().getExtras().getString("studentdetails");
//        sStudentRegNo = getIntent().getExtras().getString("studentregno");
//        tvStudentRegNo.setText( sStudentDetails);
        B = getIntent().getExtras();
        bQRGeneration =(Button)findViewById(R.id.bQRGeneration);
        bQRView =(Button)findViewById(R.id.bQRView);
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvUserName.setText(B.getString("username"));
        bQRGeneration .setOnClickListener(this);
        bQRView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.bQRGeneration):
                Intent QRGenIntent = new Intent(MenuActivity.this, QRGeneration.class);
                QRGenIntent.putExtras(B);
                QRGenIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(QRGenIntent);
                break;
            case (R.id.bQRView):
                Intent InternalIntent = new Intent(MenuActivity.this, QRView.class);
                InternalIntent .putExtras(B);
                InternalIntent .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(InternalIntent );
                break;
        }
    }
}
